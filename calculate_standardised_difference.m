
% Calculating standardised difference between Bio and Optos


%identifying the groups
%Optos = find(ROP.Annotation=='[optosstart_na]');

optos= ROPSD2(1:44,1:15);
 
%no_wrap2 = find(ROP.Annotation=='[optosstart_wrap]');
bio = ROPSD2(45:86,1:15);

%calculating mean and std for the continuous variables

%optos GROUP

mean_ga = mean(optos.GestationAtBirth,'omitnan');
std_ga = std (optos.GestationAtBirth,'omitnan');

mean_bwt = mean(optos.Birth_weight,'omitnan');
std_bwt = std (optos.Birth_weight,'omitnan');

mean_pma = mean(optos.PMAAtTest,'omitnan');
std_pma = std (optos.PMAAtTest,'omitnan');

mean_testweight = mean(optos.WeightAtTest,'omitnan');
std_testweight = std (optos.WeightAtTest,'omitnan');

mean_dur_ROP = mean(optos.DurationROPmins,'omitnan');
std_dur_ROP = std (optos.DurationROPmins,'omitnan');

mean_baseline_hr = mean(optos.BaselineMeanHR,'omitnan');
std_baseline_hr = std (optos.BaselineMeanHR,'omitnan');

mean_baseline_sats = mean(optos.BaselineMeanSats,'omitnan');
std_baseline_sats = std (optos.BaselineMeanSats,'omitnan');



% bio GROUP

nmean_ga = mean(bio.GestationAtBirth,'omitnan');
nstd_ga = std (bio.GestationAtBirth,'omitnan');

nmean_bwt = mean(bio.Birth_weight,'omitnan');
nstd_bwt = std (bio.Birth_weight,'omitnan');

nmean_pma = mean(bio.PMAAtTest,'omitnan');
nstd_pma = std (bio.PMAAtTest,'omitnan');


nmean_testweight = mean(bio.WeightAtTest,'omitnan');
nstd_testweight = std (bio.WeightAtTest,'omitnan');

nmean_dur_ROP = mean(bio.DurationROPmins,'omitnan');
nstd_dur_ROP = std (bio.DurationROPmins,'omitnan');

nmean_baseline_hr = mean(bio.BaselineMeanHR,'omitnan');
nstd_baseline_hr = std (bio.BaselineMeanHR,'omitnan');

nmean_baseline_sats = mean(bio.BaselineMeanSats,'omitnan');
nstd_baseline_sats = std (bio.BaselineMeanSats,'omitnan');

%formula to calculate standardised difference for continuous variables

%std_diff_cont = (mean(rx) - mean (ct))/(sqrt(std (rx)^2 + std (ct)^2)/2);

std_diff_GA = (mean_ga - nmean_ga)/(sqrt(std_ga^2 + nstd_ga^2)/2);
std_diff_BWT = (mean_bwt - nmean_bwt)/(sqrt(std_bwt^2 + nstd_bwt^2)/2);
std_diff_PMA = (mean_pma - nmean_pma)/(sqrt(std_pma^2 + nstd_pma^2)/2);
std_diff_TESTWEIGHT = (mean_testweight - nmean_testweight)/(sqrt(std_testweight^2 + nstd_testweight^2)/2);
std_diff_ROP_DURATION = (mean_dur_ROP - nmean_dur_ROP)/(sqrt(std_dur_ROP^2 + nstd_dur_ROP^2)/2);
std_diff_BASELINE_HR = (mean_baseline_hr - nmean_baseline_hr)/(sqrt(std_baseline_hr^2 + nstd_baseline_hr^2)/2);
std_diff_BASELINE_SATS = (mean_baseline_sats - nmean_baseline_sats)/(sqrt(std_baseline_sats^2 + nstd_baseline_sats^2)/2);

%%
%Calculating proportions for categorical variables
%gender

female_gendercount_optos=length(find(optos.Gender_recoded==0))/length(optos.Gender_recoded);
female_gendercount_bio=length(find(bio.Gender_recoded==0))/length(bio.Gender_recoded);

male_gendercount_optos=length(find(optos.Gender_recoded==1))/length(optos.Gender_recoded);
male_gendercount_bio=length(find(bio.Gender_recoded==1))/length(bio.Gender_recoded);


%formula to calculate standardised difference for categorical variable

%std_diff_cat = (prp (rx) - prp (ct))/(sqrt(prp (rx)*(1-prp (rx))) +(prp (ct)*(1-prp (ct)))/2);

std_diff_genderfem = (female_gendercount_optos - female_gendercount_bio)/(sqrt(female_gendercount_optos *(1-female_gendercount_optos)+(female_gendercount_bio*(1-female_gendercount_bio))/2));

std_diff_gendermal = (male_gendercount_optos - male_gendercount_bio)/(sqrt(male_gendercount_optos *(1-male_gendercount_optos)+(male_gendercount_bio*(1-male_gendercount_bio))/2));


%ventilation

HFTventcount_optos=length(find(optos.Ventilation_recoded==0))/length(optos.Ventilation_recoded);
HFTventcount_bio=length(find(bio.Ventilation_recoded==0))/length(bio.Ventilation_recoded);

std_diff_ventHFT = (HFTventcount_optos - HFTventcount_bio )/(sqrt(HFTventcount_optos *(1-HFTventcount_optos)+(HFTventcount_bio*(1-HFTventcount_bio))/2));

LFTventcount_optos=length(find(optos.Ventilation_recoded==1))/length(optos.Ventilation_recoded);
LFTventcount_bio=length(find(bio.Ventilation_recoded==1))/length(bio.Ventilation_recoded);

std_diff_ventLFT = (LFTventcount_optos - LFTventcount_bio )/(sqrt(LFTventcount_optos *(1-LFTventcount_optos)+(LFTventcount_bio*(1-LFTventcount_bio))/2));

SVIAventcount_optos=length(find(optos.Ventilation_recoded==2))/length(optos.Ventilation_recoded);
SVIAventcount_bio=length(find(bio.Ventilation_recoded==2))/length(bio.Ventilation_recoded);

std_diff_ventSVIA = (SVIAventcount_optos - SVIAventcount_bio )/(sqrt(SVIAventcount_optos *(1-SVIAventcount_optos)+(SVIAventcount_bio*(1-SVIAventcount_bio))/2));

