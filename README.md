# ROP vital signs
This code relates to the paper:
Ultra-widefield versus indirect: physiological responses to retinopathy of prematurity screening

Ravi Purohit, Fatima Usman, Amanda Ie, Marianne van der Vaart, Shellie Robinson, Miranda Buckle, Luke Baxter, Michell Clee, Amanda Clifford, Eleri Adams, Rebeccah Slater, Chetan K Patel, Caroline Hartley, Kanmin Xue

If you use any of this code please cite the paper.

For further help please email Prof. Caroline Hartley (caroline.hartley@paediatrics.ox.ac.uk).

Please see the comments within each code.
Code is provided which
1) Calculates the standardised difference between baseline factors (calculate_standardised_difference.m)
2) Calculates the effective number of tests and adjusted alpha significance level (bonferroni_adjustment_for_effective_number_of_independent_tests.m)
3)Code to calculate the physiological response (ie. average increase in heart rate, maximum heart rate, numbr of tachycardia etc) in each individual baby when given their vital signs recordings (ROP_metrics.m)
