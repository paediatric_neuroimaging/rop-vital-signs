function [time_brady,time_tachy,time_desat]=timesof_desatbradytachy(HR,sats,pulse,time,toplot)
%
%Function to identify desats, brady and tachy according to the definitions
%used in Poppi trial. If you use this code please cite the Poppi trial:
%   Hartley, Moultrie et al. Lancet 2018 https://doi.org/10.1016/S0140-6736(18)31813-0
%
%   Input:  HR - heart rate, recorded from the monitor, from the ECG
%           sats - oxygen saturation, recorded from the monitor
%           pulse - pulse recorded from the sats probe, this is used in
%               periods where the ECG is missing
%           time - time at which all 3 signals (HR, sats, pulse) are
%               recorded, in minutes
%           toplot - takes a value of 1 if you would like to plot the
%               signals and episodes of instability, otherwise set as zero
%   Output: time_brady - start time of bradycardia, in relation to start of input
%               data in seconds
%           time_tachy - start time of tachycardia, in relation to start of input
%               data in seconds
%           time_desat - start time of oxygen desaturation, in relation to start of input
%               data in seconds
%
% Caroline Hartley, for queries caroline.hartley@paediatrics.ox.ac.uk

time_secs=time*3600;
%%

% an episode of bradycardia will be defined as a pulse rate < 100 beats/min for at least 15 s

count=1;

all_st_brady_heartrate=[]; all_en_brady_heartrate=[];

while count<length(HR)
    
    brady_f=find(HR(count:end)<100,1,'first');
    
    if ~isempty(brady_f)
        
        en_brady=find(HR((count+brady_f-1):end)>=100,1,'first');
        
        if (time_secs(count+en_brady+brady_f-2)-time_secs(count+brady_f-1))>=15
            
            all_st_brady_heartrate=[all_st_brady_heartrate,(count+brady_f-1)];
            all_en_brady_heartrate=[all_en_brady_heartrate,(count+brady_f+en_brady-2)];
            
            count=count+brady_f+en_brady;
            
            k=1;

            while k==1
                
                brady_g=find(HR(count:end)<100,1,'first');
                
                if (time_secs(count+brady_g-2)-time_secs(count-1))<=60
                    count=count+brady_g;
                else
                    k=2;
                end
            end
            
        else 
          
            
                count=count+brady_f+en_brady; 
            
        end
        
        
        
    else
        count=length(HR);
        
    end
end

%also find events in the pulse data

count=1;

all_st_brady_pulse=[]; all_en_brady_pulse=[];

while count<length(pulse)
    
    brady_f=find(pulse(count:end)<100,1,'first');
    
    if ~isempty(brady_f)
        
        en_brady=find(pulse((count+brady_f-1):end)>=100,1,'first');
        
        if (time_secs(count+en_brady+brady_f-2)-time_secs(count+brady_f-1))>=15
            
            all_st_brady_pulse=[all_st_brady_pulse,(count+brady_f-1)];
            all_en_brady_pulse=[all_en_brady_pulse,(count+brady_f+en_brady-2)];
            
            count=count+brady_f+en_brady;
            
            k=1;

            while k==1
                
                brady_g=find(pulse(count:end)<100,1,'first');
                
                if (time_secs(count+brady_g-2)-time_secs(count-1))<=60
                    count=count+brady_g;
                else
                    k=2;
                end
            end
            
        else 
          
             
             count=count+brady_f+en_brady; 
        
               
        end
        
        
        
    else
        count=length(pulse);
        
    end
end

% an episode of tachycardia will be defined as a pulse rate > 200 beats/min for at least 15 s

count=1;

all_st_tachy_heartrate=[]; all_en_tachy_heartrate=[]; 

    
while count<length(HR)
    
    tachy_f=find(HR(count:end)>200,1,'first');
    
    if ~isempty(tachy_f)
        
        en_tachy=find(HR((count+tachy_f-1):end)<=200,1,'first');
        
        if (time_secs(count+en_tachy+tachy_f-2)-time_secs(count+tachy_f-1))>=15
            
            all_st_tachy_heartrate=[all_st_tachy_heartrate,(count+tachy_f-1)];
            all_en_tachy_heartrate=[all_en_tachy_heartrate,(count+tachy_f+en_tachy-2)];
            
            count=count+tachy_f+en_tachy;
            
            k=1;

            while k==1
                
                tachy_g=find(HR(count:end)>200,1,'first');
                
                if (time_secs(count+tachy_g-2)-time_secs(count-1))<=60
                    count=count+tachy_g;
                else
                    k=2;
                end
            end
            
        else 
          
            
             count=count+tachy_f+en_tachy; 
        
               
        end
        
        
        
    else
        count=length(HR);
        
    end
end
    

%identify episodes in pulse signal as well

count=1;

all_st_tachy_pulse=[]; all_en_tachy_pulse=[]; 

while count<length(pulse)
    
    tachy_f=find(pulse(count:end)>200,1,'first');
    
    if ~isempty(tachy_f)
        
        en_tachy=find(pulse((count+tachy_f-1):end)<=200,1,'first');
        
        if (time_secs(count+en_tachy+tachy_f-2)-time_secs(count+tachy_f-1))>=15
            
            all_st_tachy_pulse=[all_st_tachy_pulse,(count+tachy_f-1)];
            all_en_tachy_pulse=[all_en_tachy_pulse,(count+tachy_f+en_tachy-2)];
            
            count=count+tachy_f+en_tachy;
            
            k=1;

            while k==1
                
                tachy_g=find(pulse(count:end)>200,1,'first');
                
                if (time_secs(count+tachy_g-2)-time_secs(count-1))<=60
                    count=count+tachy_g;
                else
                    k=2;
                end
            end
            
        else 
          
            
            count=count+tachy_f+en_tachy; 
        
               
        end
        
        
        
    else
        count=length(pulse);
        
    end
end

% an episode of desaturation will be defined as oxygen saturation < 80% for at least 10 s

count=1;

all_st_desat=[]; all_en_desat=[]; 

while count<length(sats)
    
    desat_f=find(sats(count:end)<80,1,'first');
    
    if ~isempty(desat_f)
        
        en_desat=find(sats((count+desat_f-1):end)>=80,1,'first');
        
        if (time_secs(count+en_desat+desat_f-2)-time_secs(count+desat_f-1))>=10
            
            all_st_desat=[all_st_desat,(count+desat_f-1)];
            all_en_desat=[all_en_desat,(count+desat_f+en_desat-2)];
            
            
            count=count+desat_f+en_desat;
        
            k=1;

            while k==1

                desat_g=find(sats(count:end)<80,1,'first');

                if (time_secs(count+desat_g-2)-time_secs(count-1))<=60 %if less than this apart count as one desat
                    count=count+desat_g;
                else
                    k=2;
                end
            end
            
        else 
          
            
             count=count+desat_f+en_desat; 
        
                
        end
            
        
        
    else
        count=length(sats);
        
    end
end

%% check signal quality during episodes. remove those where there are nans in the signal
% find desats too near start of signal to check signal quality in baseline
if ~isempty(all_st_desat)
if all_st_desat(1)<21
   all_st_desat(1)=[]; all_en_desat(1)=[];
end
if all_st_desat(end)+21>length(all_st_desat)
   all_st_desat(end)=[]; all_en_desat(end)=[];
end
end

%loop through all desats to check those with nan 20 seconds before and
%after start
if ~isempty(sats)
i=1;
while i<=length(all_st_desat)
    f=length(find(isnan(sats(all_st_desat(i)-20:all_st_desat(i)+20))));
    if f>0
        all_st_desat(i)=[]; all_en_desat(i)=[];
    else
        i=i+1;
    end
end
end

%% go through brady
all_st_brady=all_st_brady_heartrate;
all_en_brady=all_en_brady_heartrate;

if ~isempty(all_st_brady)
if all_st_brady(1)<21
   all_st_brady(1)=[]; all_en_brady(1)=[];
end
end

i=1;
while i<=length(all_st_brady)
    f=length(find(isnan(HR(all_st_brady(i)-20:all_st_brady(i)+25))));
    if f>0
        all_st_brady(i)=[]; all_en_brady(i)=[];
    else
        i=i+1;
    end
end

a=all_st_brady_pulse; b=all_en_brady_pulse;

if ~isempty(a)
if a(1)<21
   a(1)=[]; b(1)=[];
end
if a(end)+25>length(HR)
    a(end)=[]; b(end)=[];
end
end

%only look at pulse if the ECG signal is lost
if ~isempty(HR)
i=1;
while i<=length(a)
    f=length(find(isnan(HR(a(i)-20:a(i)+25))));
    if f==0
        a(i)=[]; b(i)=[];
    else
        i=i+1;
    end
end
end

i=1;
while i<length(a)
    f=find(a(i)>all_st_brady,1,'last'); g=find(a(i)<=all_st_brady,1,'first');
    if a(i)<all_en_brady(f)
        a(i)=[];
        b(i)=[];
    else if b(i)>all_st_brady(g)
            a(i)=[];
            b(i)=[];
        else
            i=i+1;
        end
    end
end

%combine brady identified in ECG with those from pulse if ECG is lost
%during those periods
all_st_brady=[all_st_brady,a];
all_en_brady=[all_en_brady,b];

[all_st_brady,i]=sort(all_st_brady);
all_en_brady=all_en_brady(i);

i=1;
while i<=(length(all_st_brady)-1)
    if (time_secs(all_st_brady(i+1))-time_secs(all_en_brady(i)))<=60 
        all_st_brady(i+1)=[];
        all_en_brady(i+1)=[];
    else
        i=i+1;
    end
end

%check tachy
all_st_tachy=all_st_tachy_heartrate;
all_en_tachy=all_en_tachy_heartrate;

if ~isempty(all_st_tachy)
if all_st_tachy(1)<21
   all_st_tachy(1)=[]; all_en_tachy(1)=[];
end
end


i=1;
while i<=length(all_st_tachy)
    f=length(find(isnan(HR(all_st_tachy(i)-20:all_st_tachy(i)+25))));
    if f>0
        all_st_tachy(i)=[]; all_en_tachy(i)=[];
    else
        i=i+1;
    end
end

a=all_st_tachy_pulse; b=all_en_tachy_pulse;
if ~isempty(a)
if a(1)<21
   a(1)=[]; b(1)=[];
end
if a(end)+25>length(HR)
    a(end)=[]; b(end)=[];
end
end

%only look at pulse if the ECG signal is lost
if ~isempty(HR)
i=1;
while i<=length(a)
    f=length(find(isnan(HR(a(i)-20:a(i)+25))));
    if f==0
        a(i)=[]; b(i)=[];
    else
        i=i+1;
    end
end
end

i=1;
while i<length(a)
    f=find(a(i)>all_st_tachy,1,'last'); g=find(a(i)<=all_st_tachy,1,'first');
    if a(i)<all_en_tachy(f)
        a(i)=[];
        b(i)=[];
    else if b(i)>all_st_tachy(g)
            a(i)=[];
            b(i)=[];
        else
            i=i+1;
        end
    end
end

all_st_tachy=[all_st_tachy,a];
all_en_tachy=[all_en_tachy,b];

[all_st_tachy,i]=sort(all_st_tachy);
all_en_tachy=all_en_tachy(i);

i=1;
while i<=(length(all_st_tachy)-1)
    if (time_secs(all_st_tachy(i+1))-time_secs(all_en_tachy(i)))<=60 
        all_st_tachy(i+1)=[];
        all_en_tachy(i+1)=[];
    else
        i=i+1;
    end
end

time_desat=time(all_st_desat); %time in seconds
time_brady=time(all_st_brady);
time_tachy=time(all_st_tachy);

%% plot figures

if toplot
figure;
set(gcf, 'Position',  [50, 100, 700, 700])
sgtitle(string(fname),'fontweight','bold','fontsize',18,'Interpreter', 'none')
h1=subplot(2,1,1);
plot(time,sats);
set(gca,'fontsize',15)
ylabel('Oxygen saturation (%)','fontsize',15)
xlabel('Time (hours)','fontsize',15)
ylim([60,101])
hold on

%add desats
for j=1:length(time_desat)
    plot([time_desat(j),time_desat(j)],[60,101],'r')
end

h2=subplot(2,1,2);
if ~isempty(pulse)
plot(time,pulse,'k');
end
hold on
if ~isempty(HR)
plot(time,HR,'b')
end
set(gca,'fontsize',15)
ylabel('Heart rate (bpm)','fontsize',15)
xlabel('Time (hours)','fontsize',15)
ylim([90,220])
%add brady
for j=1:length(time_brady)
    plot([time_brady(j),time_brady(j)],[90,220],'r')
end
%add tachy
for j=1:length(time_tachy)
    plot([time_tachy(j),time_tachy(j)],[90,220],'m')
end
legend('Pulse','HR')

linkaxes([h1,h2],'x')

end