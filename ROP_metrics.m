function [max_hr,min_sats,max_rr,ave_hr_above,ave_hr_below,ave_sats_below,...
    ave_rr_above,hr_time,brady,tachy,desats,apnoea]=ROP_metrics(HR,sats,ibi,ibi_time,epochlengthpre, epochlengthpost, time_end_ROP, fs, toplot)
%
%Code to calculate metrics for changes in vital signs in relation to ROP
%screening
% The changes are calculated in short time windows in relation to the ROP
% screening. We used a baseline of 5 minutes and a period of 15 minutes
% from the start of the screen. If longer epochs are input the metrics will
% still be calculated using 15 minutes.
%
% If you use this code please cite Purohit, Usman et al.
% Code written by Caroline Hartley August 2023
% For queries please contact caroline.hartley@paediatrics.ox.ac.uk
% If you use this code to identify episodes of desaturation, bradycardia
% and tachycardia please also cite Hartley, Moultrie et al. Lancet 2018 https://doi.org/10.1016/S0140-6736(18)31813-0
% For episodes of apnoea and inter-breath intervals please also cite Adjei et al. doi: 10.1136/bmjresp-2021-001042
%
%   Input:  HR - heart rate, recorded from the monitor
%           sats - oxygen saturation, recorded from the monitor
%           ibi - inter-breath intervals - calculated using method and code from Adjei et al. doi: 10.1136/bmjresp-2021-001042
%           ibi_time - time of IBI, calculated with the associated code
%           epochlengthpre - the length of time (in minutes) of the input
%                   epoch before the start of the ROP screening
%           epochlengthpost - the length of time (in minutes) of the input
%                   epoch after the start of the ROP screening
%           time_end_ROP - the time in minutes of the end of ROP screen (same as
%               ROP screen duration, as the times are in relation to the start
%               of the ROP screen
%           fs - the sampling rate in seconds
%           toplot - 1 to plot a figure of data, 0 otherwise
%
%   Output: •	max_hr - Maximum heart rate – the maximum heart rate within the 15 minutes following the start of the screen
% •	min_sats - Minimum oxygen saturation – the minimum oxygen saturation within the 15 minutes following the start of the screen
% •	max_rr - Maximum respiratory rate – the maximum respiratory rate within the 15 minutes following the start of the screen
% •	ave_hr_above - Average heart rate increase – the area under the curve in the 15 minutes following the start of the screen above a threshold divided by the length of time (i.e. 15 minutes). The threshold was calculated as the infantas own mean plus two standard deviations of their 5 minutes of baseline (Figure xx). If the HR did not go above the baseline threshold then the average heart rate increase was 0. Similarly -
% •	ave_hr_below - Average heart rate decrease – the average area above the curve below a threshold of the baseline mean minus two standard deviations.
% •	ave_sats_below - Average oxygen saturation decrease - the average area above the curve below a threshold of the baseline mean minus two standard deviations. 
% •	ave_rr_above - Average respiratory rate increase – the average area below the curve above a threshold of the baseline mean plus two standard deviations.
% •	hr_time - Time for the heart rate to return to baseline - 
% •	brady - number of Bradycardia - periods during which the heart rate fell below 100 beats per minute for at least 15 seconds13
% •	tachy - number of Tachycardia - periods during which the heart rate rose above 200 beats per minute for at least 15 seconds13
% •	desats - numbers of Oxygen desaturation - periods during which the oxygen saturation fell below 80% for at least 10 seconds13
% •	apnoea - number of apnoeas - pauses in breathing of at least 20 seconds (i.e. IBI>=20 seconds) 


analysisepochpre=5; %time in minutes of baseline for analysis
analysisepochpost=15; %time in minutes of epoch post ROP screen start for analyis

time=-epochlengthpre:fs/60:epochlengthpost; %time in minutes

%find start times of desaturation, bradycardia and tachycardia across all
%of the data
[time_brady,time_tachy,time_desat]=timesof_desatbradytachy(HR,sats,pulse,time,toplot);

% find apnoea i.e. ibi>=20
apnoea_index=find(ibi>=20);
time_apnoea=ibi_time(apnoea_index);

%index of pre and post epochs
ind_baseline=intersect(find(time>=-analysisepochpre),find(time<0));
ind_post=intersect(find(time>=0),find(time<=analysisepochpost));
ind=[ind_baseline;ind_post];

%HR metrics
brady=length(intersect(find(time_brady>=0),find(time_brady<=analysisepochpost)));
tachy=length(intersect(find(time_tachy>=0),find(time_tachy<=analysisepochpost)));
if length(find(isnan(HR(ind))))/length(ind)>0.2 %if more than 20% of signal missing don't calculate (return nan)
        ave_hr_above=nan;
        ave_hr_below=nan;
        max_hr=nan;
        hr_time=nan;
else
    %calculate average heart rate increase
    hr_thres_max=mean(HR(ind_baseline),'omitnan')+2*std(HR(ind_baseline),'omitnan'); %heart rate threshold
    cutHR=HR(ind_post)-hr_thres_max; %cut off so get useful values in area
    ave_hr_above=sum(cutHR(find(cutHR>0)))/(epoch_length_post*60);
    
    %calculate average heart rate decrease
    hr_thres_min=mean(HR(ind_baseline),'omitnan')-2*std(HR(ind_baseline),'omitnan'); %heart rate threshold lower
    cutHR2=HR(ind_post)-hr_thres_min;
    ave_hr_below=abs(sum(cutHR2(find(cutHR2<0))))/(epoch_length_post*60);

    %calculate maximum HR
    max_hr=max(HR(ind_post));

    %calculate time for HR to return to baseline
    %use a smoothed HR so that quick dip in HR is not counted
    sHR=smooth(HR(ind),60);
    ind_post_rop=find(time(ind)>=time_end_ROP);
    ind_return_hr=find(sHR(ind_post_rop)<hr_thres_max,1,'first');
    if ~isempty(ind_return_hr)
        hr_time=time(ind(ind_post_rop(ind_return_hr)))-time_end_ROP;
    else
       hr_time=analysisepochpost-time_end_ROP; %if doesn't go back below baseline take maximum
    end
end

%sats metrics
desats=length(intersect(find(time_desat>=0),find(time_desat<=analysisepochpost)));
if length(find(isnan(sats(ind))))/length(ind)>0.2 %if more than 20% of signal missing don't calculate (return nan)
        ave_sats_below=nan;
        min_sats=nan;
else
      
    %calculate average sats decrease
    sats_thres_min=mean(sats(ind_baseline),'omitnan')-2*std(sats(ind_baseline),'omitnan'); %heart rate threshold lower
    cutsats=sats(ind_post)-sats_thres_min;
    ave_sats_below=abs(sum(cutsats(find(cutsats<0))))/(epoch_length_post*60);

    %calculate maximum sats
    min_sats=min(sats(ind_post));
end

%RR metrics
apnoea=length(intersect(find(time_apnoea>=0),find(time_apnoea<=analysisepochpost)));
if length(find(isnan(RR(ind))))/length(ind)>0.2 %if more than 20% of signal missing don't calculate (return nan)
        ave_rr_above=nan;
        max_rr=nan;
else
    %calculate average respiratory rate increase
    rr_thres_max=mean(RR(ind_baseline),'omitnan')+2*std(RR(ind_baseline),'omitnan'); %heart rate threshold
    cutRR=RR(ind_post)-rr_thres_max; %cut off so get useful values in area
    ave_rr_above=sum(cutRR(find(cutRR>0)))/(epoch_length_post*60);
    
    %calculate maximum RR
    max_rr=max(RR(ind_post));
end
